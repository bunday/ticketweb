@component('mail::message')
# New Request

Hello {{$voucher->user->first_name}},

Thank You for Signing Up and Requesting a Voucher.

We will get back to you shortly.

Your Request ID is MOV-TIK-00{{$voucher->id}}


Cheers,<br>
{{ config('app.name') }}
@endcomponent
