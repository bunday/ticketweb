@component('mail::message')
# Request Rejected

Hello {{$voucher->user->first_name}},

We are sorry to inform you that after carefully reviewing your Request, It has been rejected.

Your Request ID is MOV-TIK-00{{$voucher->id}}

The main reason for rejection is: {{ $voucher->reason }}

Cheers,<br>
{{ config('app.name') }}
@endcomponent
