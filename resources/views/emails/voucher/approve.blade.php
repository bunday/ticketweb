@component('mail::message')
# Request Approved

Hello {{$voucher->user->first_name}},

We are please to inform you that after carefully reviewing your Request, It has been granted.

Your Request ID is MOV-TIK-00{{$voucher->id}}

And your Approved Voucher Code is {{$voucher->uid}}

Cheers,<br>
{{ config('app.name') }}
@endcomponent
