@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <a href="/request" class="btn btn-success pull-right">Request a New Voucher</a>
                        <table id="listing" class="table">
                            <thead>
                            <tr class="bg-primary text-white">
                                <th>Customer Name</th>
                                <th> Voucher Code</th>
                                <th>Status</th>
                                @if(Auth::user()->category=="ADM")
                                <th>Action(s)</th>@endif
                                <th>Date Created</th>
                                <th>Last Updated</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($vouchers as $voucher)
                                <tr>
                                    <td>{{ $voucher->user->first_name }} {{ $voucher->user->last_name }}</td>
                                    <td> {{ $voucher->uid ? $voucher->uid : 'N/A'  }} </td>
                                    <td> @if($voucher->status=="0")
                                             <p class="label label-warning">Pending</p>
                                        @elseif($voucher->status=="1")
                                            <p class="label label-success">Approved</p>
                                        @else
                                            <p class="label label-danger">Rejected</p>
                                        @endif
                                    </td>
                                    @if(Auth::user()->category=="ADM")
                                    <td>
                                        @if($voucher->status=="0")
                                        <div class="row">
                                            <div class="col-md-6">
                                                <form method="post" action="/approve">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$voucher->id}}">
                                                    <button type="submit" class="btn btn-sm btn-success">
                                                        <i class="fa fa-mark"></i>Approve
                                                    </button>
                                                </form>

                                            </div>
                                            <div class="col-md-6">
                                                <button class="btn btn-sm btn-danger" onclick="reject({{$voucher->id}})" data-toggle="modal" data-target="#reject">
                                                    <i class="fa fa-times"></i>Reject
                                                </button>
                                            </div>
                                        </div>
                                            @endif
                                    </td>@endif
                                    <td> {{$voucher->created_at->format('m d, Y - h:i A')}} </td>
                                    <td> {{$voucher->updated_at->format('m d, Y - h:i A')}} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade show" id="reject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel-4">Set Reason for Rejection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="/requests/reject" >
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="col-form-label">Order ID</label>
                            <input type="text" class="form-control" id="requestId"  readonly>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="requestVid" name="requestVid">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="col-form-label">Enter Reason</label>
                            <textarea name="reason" rows="3" required class="form-control"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">
                        <i class="mdi mdi-money"></i> Reject Voucher Request
                    </button>
                </form>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        function reject(id) {
            document.getElementById('requestId').value="MOV-TIK-00"+id;
            document.getElementById('requestVid').value=id;
        }
    </script>
@endpush
