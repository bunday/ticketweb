<?php

namespace App\Http\Controllers;

use App\Http\Resources\Voucher;
use App\User;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $voucher;
    public function __construct()
    {
        $this->middleware('auth');
        $this->voucher = new VoucherController();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->category=="ADM")
            $vouchers = $this->voucher->index();
        else
            $vouchers = $this->voucher->getByAuth();

        return view('home')->with(compact('vouchers'));
    }

    public function approveRequest($id){
        $this->voucher->approve($id);
        $this->successResponse("Voucher Approved Successfully",null);
    }
    public function approve(Request $request){
        if(Auth::user()->category=="ADM")
            $this->voucher->approve($request->id);

        return redirect('/home');
    }
    public function reject(Request $request){
        $this->voucher->reject($request->requestVid, $request->reason);
        return redirect('/home');

    }

    public function request(){
        $this->voucher->store(Auth::id());
        return redirect('/home');
    }
    public function fetchRequests(){
        if(Auth::user()->category=="ADM")
            $vouchers = $this->voucher->index();
        else
            $vouchers = $this->voucher->getByAuth();

        return $this->successResponse("Voucher Request Fetched",Voucher::collection($vouchers));
    }

    public function successResponse($message,$data){
        return response()->json(['status'=>'Success','message'=>$message,'data'=>$data],200);
    }
    public function users(){
        $users = User::all();
        return view('layouts.users')->with(compact('users'));
    }
}
