<?php

namespace App\Http\Controllers;

use App\Mail\VoucherApproved;
use App\Mail\VoucherRejected;
use App\Mail\VoucherRequested;
use App\Voucher;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Mail;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Voucher::all();
    }
    public function getByAuth(){
        return Auth::user()->vouchers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        $voucher = Voucher::create([
            'user_id' => $id,
        ]);
        Mail::to($voucher->user->email)->send(new VoucherRequested($voucher));
    }

    public function getById($id){
        $voucher = Voucher::findorfail($id);
        return $voucher;
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        $voucher = $this->getById($id);
        $voucher->uid = md5(time() . rand());
        $voucher->status = "1";
        $voucher->save();

        Mail::to($voucher->user->email)->send(new VoucherApproved($voucher));
        return $voucher;
    }

    public function reject($id, $reason)
    {
        $voucher = $this->getById($id);
        $voucher->reason = $reason;
        $voucher->status = "-1";
        $voucher->save();
        Mail::to($voucher->user->email)->send(new VoucherRejected($voucher));
        return $voucher;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function edit(Voucher $voucher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voucher $voucher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voucher $voucher)
    {
        //
    }
}
