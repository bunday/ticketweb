<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class GuestController extends Controller
{
    //
    public function register(Request $request){
        $data = $request->all();
        $valid = $this->userValidator($data);
        if($valid!="true")
            return $this->errorResponse("Invalid fields in the Form",$valid);
        else {
//            $tag = time();
//            $image = $data['avatar'];
            $location = "/images/users/1539862894.png";
            $external = "http://localhost:8000/images/users/1539862894.png";
//            if(!is_null($image)){
//                $filename =  $tag. '.' . $image->getClientOriginalExtension();
//                $image->move(public_path('/images/users/'), $filename);
//                $location = '/images/users/' . $filename;
//                $external = url('/').'/images/users/' . $filename;
//            }
            $user = User::create([
                'first_name' => $data['firstName'],
                'middle_name' => $data['midName'],
                'last_name' => $data['lastName'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'avatar' => $location,
                'external' => $external,
                'password' => Hash::make($data['password']),
                'address' => $data['address'],
            ]);

            $vou = new VoucherController();
            $vou->store($user->id);
            return $this->successResponse("Account Created Successfully");
        }
    }

    public function errorResponse($message,$data){
        return response()->json(['status'=>'fail','message'=>$message,'data'=>$data],400);
    }
    public function successResponse($message){
        return response()->json(['status'=>'Success','message'=>$message],200);
    }
    public function userValidator(array $data)
    {
        $r = true;
        $validator = Validator::make($data, [
            'firstName' => 'required|string|max:255',
            'midName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6',
            'phone' => 'required|numeric|min:11',
        ]);
        if ($validator->fails()) {
            $r = $validator->messages();
        }
        return $r;
    }
}
