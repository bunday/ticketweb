<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Voucher extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reason' => $this->reason,
            'created_at' => $this->created_at->format('m d, Y - h:i A'),
            'updated_at' => $this->updated_at->format('m d, Y - h:i A'),
            'user' => new User($this->user),
            'voucher' => $this->uid,
            'status' => $this->format($this->status),
        ];
    }

    public function format($id){
        $status = "Rejected";
        if($id=="0")
            $status = "Pending";
        elseif($id=="1")
            $status = "Approved";
        else $status = "Rejected";

        return $status;
    }
}

