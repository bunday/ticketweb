<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'full_name' => $this->first_name.' '.$this->middle_name.', '.$this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'requests' => $this->vouchers->count(),
            'created_at' => $this->created_at->format('m d, Y - h:i A'),
            'category' => $this->status=="ADM" ? 'Administrator' : 'Customer',
        ];
    }
}
