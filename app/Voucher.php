<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $fillable = [
        'user_id','reason','uid'
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }
}
